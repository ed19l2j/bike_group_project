from flask import Flask,render_template, request, redirect, flash, url_for, session
from flask_wtf import FlaskForm
from wtforms import StringField,SubmitField,IntegerField,RadioField
from wtforms.validators import NumberRange
from wtforms.validators import DataRequired

app=Flask(__name__)

app.config['SECRET_KEY']='mykey'

class SelectForm(FlaskForm):
    submit=SubmitField('Rent')

class PaymentForm(FlaskForm):

    amount=IntegerField('How many tickets would you like to purhcase:',validators=[DataRequired(),NumberRange(min=1,max=20,message='')])
    length=RadioField('Please choose the time period you would like hire a scooter for', choices=[('1','1 hour'),('4','4 hours'),('24','24 hours')],validators=[DataRequired()])
    submit=SubmitField('Make payment')


@app.route('/',methods=['GET','POST'])
def index():

    form=SelectForm()
    if form.validate_on_submit():
        if 'location1' in request.form:
            return redirect(url_for('payment1'))
        elif 'location2' in request.form:
            return redirect(url_for('payment2'))

    return render_template('index.html',form=form)

@app.route('/payment1')
def payment1():
    form=PaymentForm()
    if form.validate_on_submit():
        session['amount']=form.amount.data
        session['length']=form.length.data

        return redirect(url_for('confirmation'))
    return render_template('payment.html',form=form)


@app.route('/payment2')
def payment2():
    form=PaymentForm()
    if form.validate_on_submit():
        session['amount']=form.amount.data
        session['length']=form.length.data

        return redirect(url_for('confirmation'))
    return render_template('payment2.html',form=form)






if __name__ == '__main__':
    app.run(debug=True)

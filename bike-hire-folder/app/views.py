from flask import render_template, request, redirect, flash, url_for, session
from app import app, db, models, admin, login_manager, mail
#flask-admin and login imports
from flask_admin.contrib.sqla import ModelView
from flask_login import login_required, current_user, login_user, logout_user
from datetime import datetime, date, timedelta
from .forms import *
from .models import User, CardDetails, Location, Scooter, ScooterLog, Booking, UserFeedback
import sqlite3
import datetime
from flask_mail import Mail, Message
from sqlalchemy import func
import itertools
import atexit
from apscheduler.schedulers.background import BackgroundScheduler

#admin-views - beal
admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(CardDetails, db.session))
admin.add_view(ModelView(Location, db.session))
admin.add_view(ModelView(Scooter, db.session))
admin.add_view(ModelView(ScooterLog, db.session))
admin.add_view(ModelView(Booking, db.session))
admin.add_view(ModelView(UserFeedback, db.session))

app.secret_key = 'beal'

def sensor():
	for b in models.Booking.query.all():
		if b.returnTime < datetime.datetime.now():
			for s in models.Scooter.query.all():
				if b.id == s.booking_id:
					s.availability = 1
					s.booking_id = ""
					s.location.setAvailableScooters()

				db.session.commit()

sched = BackgroundScheduler(daemon=True)
sched.add_job(sensor,'interval',seconds= 1)
sched.start()

@app.route("/")
def index():
	location1=models.Location.query.get(1)
	location2=models.Location.query.get(2)
	location3=models.Location.query.get(3)
	location4=models.Location.query.get(4)
	location5=models.Location.query.get(5)
	return render_template("locations.html",location1=location1,location2=location2,location3=location3,location4=location4,location5=location5)


@app.route("/feedback", methods=['GET','POST'])
@login_required
def feedback():
	form=FeedbackForm()

	if form.validate_on_submit():
		new_post = models.UserFeedback(title=form.title.data,text=form.text.data)
		db.session.add(new_post)
		db.session.commit()
		#flash("Blog Post Created")
		return redirect("/")

	return render_template('create_post.html',form=form)

@app.route("/admin/view_feedback")
@login_required
def adminfeedback():
	if current_user.authLevel == 0:
		return redirect('/')#not an admin
	highprio=models.UserFeedback.query.filter_by(priority=1)
	lowprio=models.UserFeedback.query.filter_by(priority=0)
	return render_template("adminfeedback.html",highprio=highprio,lowprio=lowprio)

@app.route("/updatepriodown/<int:num>", methods=['GET','POST'])
def updatehighprio(num):
	incrprio=models.UserFeedback.query.get(num)
	incrprio.priority=0;
	db.session.add(incrprio)
	db.session.commit()
	return redirect(url_for("adminfeedback"))

@app.route("/updateprioup/<int:num>", methods=['GET','POST'])
def updatelowprio(num):
	incrprio=models.UserFeedback.query.get(num)
	incrprio.priority=1;
	db.session.add(incrprio)
	db.session.commit()
	return redirect(url_for("adminfeedback"))

@app.route("/deletefeedback/<int:num>", methods=['GET','POST'])
def deleteuserfeedback(num):
	delFB=models.UserFeedback.query.get(num)
	db.session.delete(delFB)
	db.session.commit()
	return redirect(url_for("adminfeedback"))

@app.route("/admin/view_bookings", methods=['GET','POST'])
@login_required
def adminbookings():
	if current_user.authLevel == 0:
		return redirect('/')
	bookings = []
	bookingDates = []
	cancelBookings = []
	extendBookings = []
	i = 0
	for b in models.Booking.query.all():

		date = b.collectTime.strftime("%m") + "/" + b.collectTime.strftime("%d") + "/" + b.collectTime.strftime("%Y")
		if len(bookingDates) == 0:
			bookingDates.append(date)
		if bookingDates[i] != date:	
			bookingDates.append(date)
			booking = []
			booking.append(b.numScooters)#1
			booking.append(b.cost)#2
			booking.append(b.hireTime)#3
			if b.returnTime.day > datetime.datetime.now().day:
				#4
				booking.append(b.returnTime.strftime("%m") + "/" + b.returnTime.strftime("%d") + " at " + b.returnTime.strftime("%H") + ":" + b.returnTime.strftime("%M"))
			else:
				booking.append(b.returnTime.strftime("%H") + ":" + b.returnTime.strftime("%M"))
			booking.append(date)#5
			booking.append(b.user.email)#6
			booking.append(b.user.firstName)#7
			if not b.scooters:
				booking.append(0)
			else:
				booking.append(b.scooters[0].location.locationName)
			bookings.append(booking)
			i=i+1
		else:
			booking = []
			booking.append(b.numScooters)#1
			booking.append(b.cost)#2
			booking.append(b.hireTime)#3
			if b.returnTime.day > datetime.datetime.now().day:
				#4
				booking.append(b.returnTime.strftime("%m") + "/" + b.returnTime.strftime("%d") + " at " + b.returnTime.strftime("%H") + ":" + b.returnTime.strftime("%M"))
			else:
				booking.append(b.returnTime.strftime("%H") + ":" + b.returnTime.strftime("%M"))
			booking.append(date)#5
			booking.append(b.user.email)#6
			booking.append(b.user.firstName)#7
			if not b.scooters:
				booking.append(0)
			else:
				booking.append(b.scooters[0].location.locationName)
			bookings.append(booking)

	if request.method == 'POST':
		if "cancel" in request.form["submit"]:
			for b in cancelBookings:
				if str(b.id) in request.form["submit"]:
					for s in b.scooters:
						s.availability = 1
						s.location.setAvailableScooters()
					db.session.delete(b)
					db.session.commit()
					return redirect(url_for("adminbookings"))
		if "extend" in request.form["submit"]:
			for b in extendBookings:
				if str(b.id) in request.form["submit"]:
					b.returnTime += datetime.timedelta(minutes=60*int(request.form.get("extendTime")))
					#change cost depending on increased time
					db.session.add(b)
					db.session.commit()
					return redirect(url_for("adminbookings"))

	return render_template("adminbookings.html", bookings=bookings, bookingDates=bookingDates);

@app.route("/profile/bookings", methods=['GET','POST'])
@login_required
def userbookings():
	bookings = []
	bookingDates = []
	cancelBookings = []
	extendBookings = []
	i = 0
	for b in current_user.bookings.all():
		date = b.collectTime.strftime("%m") + "/" + b.collectTime.strftime("%d") + "/" + b.collectTime.strftime("%Y")
		if len(bookingDates) == 0:
			bookingDates.append(date)
		if bookingDates[i] != date:	
			bookingDates.append(date)
			booking = []
			booking.append(b.numScooters)
			booking.append(b.cost)
			booking.append(b.hireTime)
			if b.returnTime.day > datetime.datetime.now().day:
				booking.append(b.returnTime.strftime("%m") + "/" + b.returnTime.strftime("%d") + " at " + b.returnTime.strftime("%H") + ":" + b.returnTime.strftime("%M"))
			else:
				booking.append(b.returnTime.strftime("%H") + ":" + b.returnTime.strftime("%M"))
			booking.append(date)
			if datetime.datetime.now() < b.collectTime:
				booking.append(1)#cancellation boolean
				cancelBookings.append(b)
			else:
				booking.append(0)
			booking.append(b.id)
			if not b.scooters:
				booking.append(0)
			else:
				booking.append(b.scooters[0].location.locationName)
			if datetime.datetime.now() < b.returnTime and b.returnTime < datetime.datetime.now() + datetime.timedelta(minutes=10079):
				booking.append(1)#extend bookings boolean
				extendBookings.append(b)
			else:
				booking.append(0)
			booking.append(b.card_digits)
			bookings.append(booking)
			i=i+1
		else:
			booking = []
			booking.append(b.numScooters)
			booking.append(b.cost)
			booking.append(b.hireTime)
			#if the returntime is past tomorrow
			if b.returnTime.day > datetime.datetime.now().day:
				booking.append(b.returnTime.strftime("%m") + "/" + b.returnTime.strftime("%d") + " at " + b.returnTime.strftime("%H") + ":" + b.returnTime.strftime("%M"))
			else:
				booking.append(b.returnTime.strftime("%H") + ":" + b.returnTime.strftime("%M"))
			booking.append(date)
			#if collect time is in the future
			if datetime.datetime.now() < b.collectTime:
				booking.append(1)#cancellation boolean
				cancelBookings.append(b)
			else:
				booking.append(0)
			booking.append(b.id)
			if not b.scooters:
				booking.append(0)
			else:
				booking.append(b.scooters[0].location.locationName)
			if datetime.datetime.now() < b.returnTime and b.returnTime < datetime.datetime.now() + datetime.timedelta(minutes=10079):
				booking.append(1)#extend bookings boolean
				extendBookings.append(b)
			else:
				booking.append(0)
			booking.append(b.card_digits)
			bookings.append(booking)

	if request.method == 'POST':
		if "cancel" in request.form["submit"]:
			for b in cancelBookings:
				if str(b.id) in request.form["submit"]:
					for s in b.scooters:
						s.availability = 1
						s.location.setAvailableScooters()
					db.session.delete(b)
					db.session.commit()
					return redirect(url_for("userbookings"))
		if "extend" in request.form["submit"]:
			for b in extendBookings:
				if str(b.id) in request.form["submit"]:
					b.hireTime = b.hireTime + int(request.form.get("extendTime"))
					b.returnTime += datetime.timedelta(minutes=60*int(request.form.get("extendTime")))
					#change cost depending on increased time
					db.session.add(b)
					db.session.commit()
					return redirect(url_for("userbookings"))

	return render_template("userbookings.html", bookings=bookings, bookingDates=bookingDates);

@app.route("/hire", methods  = ["GET","POST"])
@login_required
def hire():

	session['location'] = request.args.get('location')
	print(request.args.get('location'))
	if session['location'] == None:
		session['location'] = "Trinity Centre"
	loc = models.Location.query.filter_by(locationName=session.get('location')).first()

	session['confirm'] = 1
	if (loc.numAvailableScooters == 0):
		session['confirm']= 0

	print(loc.numAvailableScooters)
	returnString = ""
	prompt = ""
	#later add logged in requirement
	if request.method == 'POST':
		if request.form["submit"] == "Confirm":
			booking = models.Booking()#some booking object to store this booking - remove if the booking is never confirmed
			#£1 for reservation per scooter
			cost = 1

			returntime = datetime.datetime.now()#default the return time to now, add to it later
			collecttime = datetime.datetime.now() + datetime.timedelta(minutes=15)
			if request.form.get("hireTime") == "1hour":
				returntime += datetime.timedelta(minutes=60+15)#add an hour and 15 to return time (15 minutes to collect, 1 hour to use)
				returnString = returntime.strftime("%H") + ":" + returntime.strftime("%M")#create a string to display return time
				cost += 5#add £5 cost
				booking.hireTime = 1#set this booking's hire time to one hour
			elif request.form.get("hireTime") == "4hour":
				returntime += datetime.timedelta(minutes=240+15)
				returnString = returntime.strftime("%H") + ":" + returntime.strftime("%M")
				cost += 20
				booking.hireTime = 4
			elif request.form.get("hireTime") == "24hour":
				returntime += datetime.timedelta(minutes=1440+15)
				returnString = returntime.strftime("%m") + "/" + returntime.strftime("%d") + " " + returntime.strftime("%H") + ":" + returntime.strftime("%M")
				cost += 40
				booking.hireTime = 24
			elif request.form.get("hireTime") == "1week":
				returntime += datetime.timedelta(minutes=10080+15)
				returnString = returntime.strftime("%m") + "/" + returntime.strftime("%d") + " " + returntime.strftime("%H") + ":" + returntime.strftime("%M")
				cost += 150
				booking.hireTime = 168

			cost = cost * int(request.form.get("numScooters"))

			#fill in the rest of the data for the booking
			booking.cost = cost
			booking.numScooters = request.form.get("numScooters")
			print(request.form.get("numScooters"))
			booking.collectTime = collecttime
			booking.returnTime = returntime
			booking.confirmed = 0

			b = current_user.bookings.order_by(models.Booking.id.desc()).first()
			if b != None:
				if b.confirmed == 0:
					print("Test\n")
					prompt = "You already have a pending booking. Would you like to continue this booking or submit this booking instead?"
					if request.form.get("continue"):#continue with current booking
						db.session.delete(b)
						db.session.commit()
					elif request.form.get("cancel"):#discard current booking, use previous pending booking
						session['collectString'] = b.collectTime.strftime("%H") + ":" + b.collectTime.strftime("%M")
						session['returnString'] = returnString
						session['cost'] = str(b.cost)
						return redirect("/hire/confirm")

			#if continuing with current booking...
		current_user.bookings.append(booking)

		db.session.add(booking)
		db.session.commit()

		collectString = collecttime.strftime("%H") + ":" + collecttime.strftime("%M")
		session['collectString'] = collectString
		session['returnString'] = returnString
		session['cost'] = str(cost)



		return redirect("/hire/confirm")
	return render_template("hire.html", prompt=prompt, numAvailableScooters=loc.numAvailableScooters)

@app.route("/hire/confirm", methods = ["GET", "POST"])
@login_required
def confirmBooking():
	#current logic: store the booking in hire(), then move to here on submitting 'confirm'
	#then, delete the booking, since it has not been confirmed, but store the booking object locally beforehand
	#then when the user clicks confirm, store this booking in the database again
	#this prevents passing the booking object from hire() to here, cause i can't figure that out right now
	#also prevents flooding the database with a bunch of bookings that were never actually confirmed - beal
	# booking = models.Booking.query.order_by(-models.Booking.id).first()#store the most recent booking
	# db.session.delete(booking)#remove it, we have it locally above
	# db.session.commit()

	if session.get("confirm") == 0:
		return redirect('/')


	#print(request.form.get("pay"))
	if request.method == 'POST':
		if request.form["submit"] == "pay":
			return redirect('/pay/savedCards')
#no redirect just yet - need a booking page that shows price and payment and all that after booking - like a receipt

	returnString = session.get('returnString', None)
	collectString = session.get('collectString', None)
	cost = session.get('cost', None)
	return render_template("confirmBooking.html", collectString=collectString, returnString=returnString, cost=cost)

@app.route("/test")
def test():
	#remove later - for testing purposes
	return render_template("userbasetest.html")

@app.route("/about")
def about():
	return render_template("about.html")

@app.route("/login", methods = ["GET","POST"])
def login():
	if current_user.is_authenticated:
		#if already logged in
		return redirect('/')
	#otherwise
	error = ""
	form = LogIn()
	if len(models.User.query.all()) == 0:
		#if there are no users, redirect for signup - only really relevant in testing
		return redirect(url_for('signup'))
	#otherwise, when form is submitted
	elif form.validate_on_submit():
		if models.User.query.filter_by(email=form.email.data).first() is not None:
			#if the email exists for a user
			user = models.User.query.filter_by(email=form.email.data).first()
			if user.check_password_hash(form.password.data):
				#if the password is incorrect or the email is not found
				#******* implement some error message *******
				error = "Incorrect password."
				return render_template("login.html", form=form, error=error)
			login_user(user, remember=True)
		else:
			error = "Email not recognised."
			return render_template("login.html", form=form, error=error)
		return redirect(url_for('index'))
	return render_template("login.html", form=form, error=error)

@app.route("/admin/view_statistics",methods = ["GET","POST"])
def adminlogin():
	print("hello")
	if current_user.authLevel != 2:
		print(current_user.authLevel)
		return redirect('/')
	form = Date()
	today = date.today()

	if form.validate_on_submit():
		today = form.date.data

	totalDay = [0, 0, 0, 0, 0]
	totalWeek = [0,0,0,0,0]
	transactions =  models.Booking.query.filter((func.date(Booking.collectTime) == today),Booking.hireTime == 1).all()
	for transaction in transactions:
		totalDay[0] = transaction.cost + totalDay[0]

	transactions =  models.Booking.query.filter((func.date(Booking.collectTime) == today),Booking.hireTime == 4).all()
	for transaction in transactions:
		totalDay[1] = transaction.cost +  totalDay[1]
		 
	transactions =  models.Booking.query.filter((func.date(Booking.collectTime) == today),Booking.hireTime == 24).all()
	for transaction in transactions:
		totalDay[2] = transaction.cost + totalDay[2]

	transactions =  models.Booking.query.filter((func.date(Booking.collectTime) == today),Booking.hireTime == 168).all()
	for transaction in transactions:
		totalDay[3] = transaction.cost + totalDay[3]

	weekNum = today.isocalendar()[1]
	transactions =  models.Booking.query.filter(func.extract('week', func.date(Booking.collectTime)) == weekNum,Booking.hireTime == 1 ).all()
	for transaction in transactions:
		totalWeek[0] = transaction.cost + totalWeek[0]

	transactions =  models.Booking.query.filter(func.extract('week', func.date(Booking.collectTime)) == weekNum,Booking.hireTime == 4 ).all()
	for transaction in transactions:
		totalWeek[1] = transaction.cost + totalWeek[1]

	transactions =  models.Booking.query.filter(func.extract('week', func.date(Booking.collectTime)) == weekNum,Booking.hireTime == 24 ).all()
	for transaction in transactions:
		totalWeek[2] = transaction.cost + totalWeek[2]

	transactions =  models.Booking.query.filter(func.extract('week', func.date(Booking.collectTime)) == weekNum,Booking.hireTime == 168 ).all()
	for transaction in transactions:
		totalWeek[3] = transaction.cost + totalWeek[3]


	weekTotal = [0] * 7
	startDay = today - timedelta(days=today.weekday())
	for i in range(0,7):
		endDay = startDay + timedelta(days=i)
		transactions =  models.Booking.query.filter((func.date(Booking.collectTime) == endDay)).all()
		for transaction in transactions:
			weekTotal[i] = transaction.cost + weekTotal[i]
	weekTotal = list(itertools.accumulate(weekTotal))
	yearNum = today.isocalendar()[0]
	totalYear = [0] * 54
	totalYear1H = [0] * 54
	totalYear4H = [0] * 54
	totalYear1D = [0] * 54
	totalYear1W = [0] * 54
	weekRange = [0] * 54
	for i in range(0,54):
		weekRange[i] = i;
		x = (i -1)
		if(i == 0):
			x = 0
		transactions =  models.Booking.query.filter(func.extract('week', func.date(Booking.collectTime)) == i,func.extract('year', func.date(Booking.collectTime)) == yearNum).all()
		for transaction in transactions:
			totalYear[i] = transaction.cost + totalYear[i]
		transactions =  models.Booking.query.filter(func.extract('week', func.date(Booking.collectTime)) == i,func.extract('year', func.date(Booking.collectTime)) == yearNum,Booking.hireTime == 1).all()
		for transaction in transactions:
			totalYear1H[i] = transaction.cost + totalYear1H[i]
		transactions =  models.Booking.query.filter(func.extract('week', func.date(Booking.collectTime)) == i,func.extract('year', func.date(Booking.collectTime)) == yearNum,Booking.hireTime == 4).all()
		for transaction in transactions:
			totalYear4H[i] = transaction.cost + totalYear4H[i]
		transactions =  models.Booking.query.filter(func.extract('week', func.date(Booking.collectTime)) == i,func.extract('year', func.date(Booking.collectTime)) == yearNum,Booking.hireTime == 24).all()
		for transaction in transactions:
			totalYear1D[i] = transaction.cost + totalYear1D[i]
		transactions =  models.Booking.query.filter(func.extract('week', func.date(Booking.collectTime)) == i,func.extract('year', func.date(Booking.collectTime)) == yearNum,Booking.hireTime == 168).all()
		for transaction in transactions:
			totalYear1W[i] = transaction.cost + totalYear1W[i]

	totalYear = list(itertools.accumulate(totalYear))
	totalYear1H = list(itertools.accumulate(totalYear1H))
	totalYear4H = list(itertools.accumulate(totalYear4H))
	totalYear1D = list(itertools.accumulate(totalYear1D))
	totalYear1W = list(itertools.accumulate(totalYear1W))
	weeklySum = sum(totalWeek)
	dailySum = sum(totalDay)
	weekName = ["Mon","Tue","Wed","Thur","Fri","Sat","Sun"]
	return render_template("stats.html",totalDay = totalDay,dailySum = dailySum ,form=form,today=today, weekNum = weekNum,totalWeek = totalWeek,weeklySum = weeklySum,totalYear = totalYear, yearNum = yearNum,
						totalYear1W = totalYear1W, totalYear1H = totalYear1H, totalYear4H = totalYear4H, totalYear1D = totalYear1D,weekRange = weekRange, weekTotal = weekTotal)



@app.route("/logout")
@login_required
def logout():
	logout_user()
	return redirect("/login")

@app.route("/signup", methods = ["GET","POST"])
def signup():
	error = ""
	form = SignUp()
	if form.validate_on_submit():
		if models.User.query.filter_by(email=form.email.data).first() is None:
			new_user = models.User(email=form.email.data,
										firstname=form.forename.data,
										lastname=form.surname.data,
										password=form.password.data)
			db.session.add(new_user)
			db.session.commit()
			# forename = request.form["forename"]
			# surname = request.form["surname"]
			# email = request.form["email"]
			# password = request.form["password"]
			login_user(new_user)
		else:
			error = "Email already in use."
			return render_template("signup.html", form=form, error=error)
		return redirect("/")
	return render_template("signup.html", form=form, error=error)

@app.route("/pay/newCard", methods = ["GET", "POST"])
@login_required
def payment():
	error = ""
	cost = session.get("cost", None)
	form = CardDetailsForm()
	if form.validate_on_submit():
		card = models.CardDetails()
		if card.setAccountNumber(form.accNum.data) and card.setCVC(form.cvc.data):
			for c in current_user.cards:
				if c.checkAccountNumber(form.accNum.data):
					error = "You already have a card saved with this details!"
					return redirect("/pay/savedCards?error=You%20already%20have%20a%20card%20saved%20with%20these%20details!")
			booking = current_user.bookings.order_by(models.Booking.id.desc()).first()
			# return redirect('payment.html')
			#print(booking.findAvailableScooter(session.get('location')))
			for s in range(0, booking.numScooters):

				if booking.findAvailableScooter(session.get('location')) == 1:
					booking.confirmed = 1
					card.accName = form.accName.data
					card.expDate = form.expDate.data
					if request.form.get("saveCard"):
						current_user.cards.append(card)
					booking.card_digits = card.accNum_digits
					db.session.add(booking)
					db.session.commit()
				else:
					flash("No scooters available! Please check back later.")
			msg = Message('Booking Confirmation from Just Scoot', sender = 'justscootbookings@gmail.com', recipients = [current_user.email])
			msg.body = "Your Booking has been confirmed!\nYour collection time is " + str(booking.collectTime.strftime("%H")) + ":" + str(booking.collectTime.strftime("%M")) + ".\nYou have booked " + str(booking.numScooters) + " scooter(s)." + "\nPlease return these before " + booking.returnTime.strftime("%m") + "/" + booking.returnTime.strftime("%d") + " " + booking.returnTime.strftime("%H") + ":" + booking.returnTime.strftime("%M") + ".\nThis order has a cost of £" + str(booking.cost) + "."
			mail.send(msg)
			return redirect('/profile/bookings')
		else:
			error = "Invalid card number or cvc!"
	return render_template('payment.html', form=form, cost=cost, error=error)

@app.route("/pay/savedCards", methods = ["GET", "POST"])
@login_required
def paySavedCards():
	error = request.args.get("error")
	if error == None:
		error = ""
	if len(current_user.cards) == 0:
		return redirect("/pay/newCard")
	if request.method == "POST":
		for c in current_user.cards:
			cardID = "savedCard" + str(c.id)
			# print("test1")
			if request.form["submit"] == str(c.id):
				# print(c.checkCVC("123"))
				if c.checkCVC(request.form["cvc"]):
					# print("test3")
					booking = current_user.bookings.order_by(models.Booking.id.desc()).first()
					# return redirect('payment.html')
					#print(booking.findAvailableScooter(session.get('location')))
					for s in range(0, booking.numScooters):

						if booking.findAvailableScooter(session.get('location')) == 1:
							booking.confirmed = 1
							booking.card_digits = c.accNum_digits
							db.session.add(booking)
							db.session.commit()
						else:
							flash("No scooters available! Please check back later.")

					msg = Message('Booking Confirmation from Just Scoot', sender = 'justscootbookings@gmail.com', recipients = [current_user.email])
					msg.body = "Your Booking has been confirmed!\nYour collection time is " + str(booking.collectTime.strftime("%H")) + ":" + str(booking.collectTime.strftime("%M")) + ".\nYou have booked " + str(booking.numScooters) + " scooter(s)." + "\nPlease return these before " + booking.returnTime.strftime("%m") + "/" + booking.returnTime.strftime("%d") + " " + booking.returnTime.strftime("%H") + ":" + booking.returnTime.strftime("%M") + ".\nThis order has a cost of £" + str(booking.cost) + "."
					mail.send(msg)
					return redirect('/profile/bookings')
				else:
					error = "Incorrect cvc!"
	return render_template("paymentSavedCard.html", error=error)

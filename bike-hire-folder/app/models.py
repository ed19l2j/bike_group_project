from app import db, login_manager
from flask_login import UserMixin
from datetime import datetime
#imported for user password hashing
from werkzeug.security import generate_password_hash, \
	check_password_hash
import hashlib


Base = db.Model # used in assosciation tables for many-to-many relationships- beal

# !--------ASSOSCIATION TABLES --------!
#needed for many-to-many relationships

#relationships:
#many users can have many cards
#one log can have one scooter
#one scooter may be in many logs
#one log can have one location
#one location may be included in many logs
#one scooter can have one location (at a time)

user_card_association_table = db.Table('user_to_card', Base.metadata,
  db.Column('card_id', db.Integer, db.ForeignKey('card_details.id')),
  db.Column('user_id', db.Integer, db.ForeignKey('users.id'))
)

@login_manager.user_loader
def load_user(user_id):
	return User.query.get(int(user_id))

# !--------CLASSES--------!
class UserFeedback(Base):
	__tablename__='userfeedback2'
	id = db.Column(db.Integer, primary_key=True)
	title = db.Column(db.String(140), nullable=False)
	text = db.Column(db.Text, nullable=False)
	date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
	priority=db.Column(db.Integer, default=0)

	def __init__(self, title, text):
		self.title = title
		self.text = text


class Booking(Base):
	__tablename__ = 'bookings'
	id = db.Column(db.Integer, primary_key=True)

	user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
	user = db.relationship('User')

	cost = db.Column(db.Integer)
	numScooters = db.Column(db.Integer)
	hireTime = db.Column(db.Integer)
	collectTime = db.Column(db.DateTime)
	returnTime = db.Column(db.DateTime)
	confirmed = db.Column(db.Integer, default=0)#
 
	scooters = db.relationship('Scooter', back_populates='booking')

	card_digits = db.Column(db.Integer)

	def findAvailableScooter(self, locationName):
		loc = Location.query.filter_by(locationName=locationName).first()
		if loc == None:
			return -1
		for s in loc.scooters:
			if s.availability == 1:
				self.scooters.append(s)
				s.availability = 0
				loc.setAvailableScooters()
				return 1
		return 0

class User(UserMixin, Base):
	__tablename__ = 'users'
	id = db.Column(db.Integer, primary_key=True)
	email = db.Column(db.String(100), index=True, unique=True)
	#modification: removed unnecessary 'index=True's - think they end up breaking the DB- beal
	firstName = db.Column(db.String(100))
	lastName = db.Column(db.String(100))
	#modification: changed variable name- beal
	password_hash = db.Column(db.String(100))
	authLevel = db.Column(db.Integer, default=0) #need some way of setting this without just a checkbox for admin or not  - beal

	#modification: added card details attribute & many-to-many relationship- beal

	cards = db.relationship('CardDetails', back_populates='user')
	bookings = db.relation('Booking', back_populates='user', lazy='dynamic')

	def __init__(self, email, firstname, lastname, password):
		self.email = email
		self.firstName = firstname
		self.lastName = lastname
		self.password = self.set_password(password)

	#should be passed a 'Card' object in views.py, appends it to the user's 'cards' relationship
	#used by 'User.addCardDetails(card)'
	#def addCardDetails(self, card):
	#	self.cards.append(card)

	def __repr__(self):
		return '{}{}{}{}{}'.format(self.id, self.email, self.firstName, self.lastName, self.authLevel)

	def check_password_hash(self, password):
		if generate_password_hash(password) == self.password_hash:
			return True
		return False

	#should be passed a string for a password
	#used by 'User.set_password(password)', most likely in views.py- beal
	def set_password(self, password):
		self.password_hash = generate_password_hash(password)


class CardDetails(Base):
	__tablename__ = 'card_details'
	id = db.Column(db.Integer, primary_key=True)
	accName = db.Column(db.String(100), index=True)
	accNum_hash = db.Column(db.Integer()) # hash the account number, like we would a password
	accNum_digits = db.Column(db.String(4)) # store the last 4 digits of the number
	expDate = db.Column(db.DateTime)
	#modification: added cvc - can use like a password before topping balance - beal
	cvc_hash = db.Column(db.String(265))

	#modification: added users var for many-to-many relationship - beal
	user_id = db.Column(db.Integer, db.ForeignKey('users.id'))
	user = db.relationship('User')
	# def __init__(self, accName, accNum, expDate, cvc):
	# 	self.accName = accName
	# 	self.accNum_hash = self.setAccountNumber(accNum)
	# 	self.expDate = expDate
	# 	self.cvc_hash = self.setCVC(cvc)

	#used by 'CardDetails.setAccountNumber(accNum)' - beal
	def setAccountNumber(self, accNum):
		if(len(str(accNum)) == 16):
			self.accNum_hash = hashlib.md5(str(accNum).encode()).hexdigest()#hash account number
			self.accNum_digits = str(accNum)[-4:]# store last 4 digits of account number
			return True
		else:
			return False

	def checkAccountNumber(self, accNum):
		if hashlib.md5(str(accNum).encode()).hexdigest() == self.accNum_hash:
			return True
		return False

	#used by 'CardDetails.setCVC(cvc)' - beal
	def setCVC(self, cvc):
		if(len(str(cvc)) == 3):
			self.cvc_hash = hashlib.md5(str(cvc).encode()).hexdigest()#hash cvc
			return True
		else:
			return False

	#used to verify payment - used by 'CardDetails.checkCVC(cvc)' - beal
	def checkCVC(self, cvc):
		if hashlib.md5(str(cvc).encode()).hexdigest() == self.cvc_hash:
			return True
		return False

	def __repr__(self):
		# possible security issue here - need to figure out what should be returned (if anything other than id)- beal
		return '{}{}{}'.format(self.id, self.accName, self.accNum_digits)

#modification: changed class name to be singular- beal
class Scooter(Base):
	__tablename__ = 'scooters'
	id = db.Column(db.Integer, primary_key=True)

	booking_id = db.Column(db.Integer, db.ForeignKey('bookings.id'))
	booking = db.relationship('Booking', back_populates='scooters')

	availability = db.Column(db.Integer)
	location_id = db.Column(db.Integer, db.ForeignKey('locations.id'))
	location = db.relationship('Location', back_populates='scooters')
	#modification: added logs var - need it for foreign key to work in Log()- beal
	log = db.relationship('ScooterLog', back_populates='scooter')#need uselist=False for one-to-one relationship

	def setAvailability(self, availability):
		self.availability = availability

#modification: changed class name to be singular- beal
class Location(Base):
	__tablename__ = 'locations'
	id = db.Column(db.Integer, primary_key=True)
	address = db.Column(db.String(100), index=True)
	locationName = db.Column(db.String(200))

	scooters = db.relationship('Scooter', back_populates='location')

	numAvailableScooters = db.Column(db.Integer)#modification: changed var name to be clearer- beal
	log = db.relationship('ScooterLog', back_populates='location')#need uselist=False for one-to-one relationship

	def setAvailableScooters(self):
		n = 4
		for s in self.scooters:
			if s.availability == 0:
				n = n - 1
		self.numAvailableScooters = n
		return 1


	def listAvailableScooters(self):
		Scooters = []
		for s in self.scooters:
			if s.availability == 1:
				Scooters.append(s)
		return Scooters


#modification: changed class name to refer to scooters only be singular- beal
class ScooterLog(Base):
	__tablename__ = 'scooter_logs'
	id = db.Column(db.Integer, primary_key=True)
	startDate = db.Column(db.DateTime)  #not sure how we are doing this one exactly- lewis
	#modification: changed next two vars to foreign keys- beal
	scooter = db.relationship('Scooter')
	scooter_id = db.Column(db.Integer, db.ForeignKey('scooters.id'))
	location = db.relationship('Location')
	location_id = db.Column(db.Integer, db.ForeignKey('locations.id'))

	action = db.Column(db.Integer)
	#modification: added description of action, we can set an action number when logging something, then store what it did as a description- beal
	actionDesc = db.Column(db.String(100))

	def __init__(self, startDate, scooter, location, action):
		self.startDate = startDate
		self.scooter = scooter
		self.location = location
		self.action = self.setAction(action) #set action and description together - beal

	def setAction(self, action):
		if action == 0:
			actionDesc = 'Reserve'
			self.action = action
			return True
		elif action == 1:
			actionDesc = 'Hire'
			self.action = action
			return True
		elif action == 2:
			actionDesc = 'Return'
			self.action = action
			return True
		else:
			return False #invalid action


	def __repr__(self):
		return '{}{}{}{}{}{}'.format(self.id, self.startDate, self.scooter, self.location, self.action, self.actionDesc)

def init_db():
	db.create_all()
	if User.query.filter_by(email="justscootbookings@gmail.com").first() is None:
		user = User("justscootbookings@gmail.com", "Admin", "Account", "admin")
		user.authLevel = 1
		db.session.add(user)
		db.session.commit()
		user = User("Owner@justscoot.com", "Admin", "Account", "admin")
		user.authLevel = 2
		db.session.add(user)
		db.session.commit()
	if len(Location.query.all()) == 0:
		locNames = ["Trinity Centre", "Train Station", "Merrion Centre", "LRI Hospital", "UoL Edge Sports Centre"]

		for l in range(0, 5):
			loc = Location()
			loc.locationName = locNames[l]
			loc.address = "address" + str(l)

			for i in range(0, 4):
				s = Scooter()
				s.availability = 1
				s.location = loc
				db.session.add(s)
			loc.setAvailableScooters()
			db.session.add(loc)

		db.session.commit()



init_db()

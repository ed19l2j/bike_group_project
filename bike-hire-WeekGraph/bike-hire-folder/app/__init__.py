from flask import Flask
from flask_admin import Admin
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_mail import Mail, Message

app = Flask(__name__)
app.config.from_object('config')
db = SQLAlchemy(app)
mail = Mail(app)

app.config['MAIL_SERVER']='smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'justscootbookings@gmail.com'
app.config['MAIL_PASSWORD'] = 'Justscootbookings1.'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True


#flask-admin handling
login_manager = LoginManager(app)
login_manager.init_app(app)
login_manager.login_view = 'login'

migrate = Migrate(app, db, render_as_batch=True)
admin = Admin(app, template_mode='bootstrap3')

if __name__ == '__main__':
	app.run(debug=True)

from app import views, models

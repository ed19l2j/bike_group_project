from app import app
from flask_wtf import FlaskForm, Form
from wtforms.fields import DateField, EmailField
from wtforms import IntegerField, StringField, DateField, widgets, SelectField, PasswordField, FloatField, TextAreaField
from wtforms.validators import InputRequired, length, Length, Email, EqualTo, NoneOf, DataRequired, NumberRange

error_None = "This field is required"
#created example form for inputting user data - beal
class UserForm(FlaskForm):
	email = EmailField('email', validators=[DataRequired()])
	firstName = IntegerField('firstName', validators=[DataRequired()])
	lastName = IntegerField('lastName', validators=[DataRequired()])
	#'PasswordField' hides the input like a real password input thing would
	password = PasswordField('password', validators=[DataRequired()])

class CardDetailsForm(FlaskForm):
	accName = StringField('accName', validators=[DataRequired()])
	accNum = IntegerField('accNum', validators=[DataRequired()])
	expDate = DateField('expDate', validators=[DataRequired()])
	cvc = IntegerField('cvc', validators=[DataRequired(), NumberRange(min=0, max=999)])

class LogIn(FlaskForm):
	email = StringField("Email", validators=[InputRequired(message=error_None)])
	password = PasswordField("Password", validators=[InputRequired(message=error_None)])

class SignUp(FlaskForm):
	emails = []
	forename = StringField("Forename", validators = [InputRequired(message=error_None), length(min=2, max=20, message="Invalid forename")])
	surname = StringField("Surname", validators = [InputRequired(message=error_None), length(min=2, max=20, message="Invalid Surname")])
	#email = StringField("Email", validators = [InputRequired(message=error_None), Email(message="Invalid e-mail")])
	email = StringField("Email", validators = [InputRequired(message=error_None), Email(message="Invalid e-mail"),
												NoneOf(emails, message="E-mail is already in use")])
	password = PasswordField("Password", validators = [InputRequired(message=error_None), length(min=5, message="Password too short"),
																						length(max=20, message="Password too long")])
	confirm = PasswordField("Confirm", validators = [InputRequired(message=error_None), EqualTo("password", message="Passwords must match")])


class FeedbackForm(FlaskForm):
	title = StringField('Title', validators=[DataRequired()])
	text = TextAreaField('Text', validators=[DataRequired()])

class Date(FlaskForm):
    date = DateField('Date',validators=[InputRequired(message=error_None)])